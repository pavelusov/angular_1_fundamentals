'use strict';

eventsApp.controller('EventController',
    function EventController($scope) {

        $scope.event = {
            name: 'Event Fundamentals',
            date: '18 mart 2017',
            time: '12:00',
            location: {
                address: 'Google Headquarters',
                city: 'Mountain View',
                province: 'CA'
            },
            imageURL: 'img/angularjs-logo.png',
            sessions: [
                {
                    name: 'Directive Masterclass',
                    level: 'Advanced',
                    upVoteCount: 0
                },
                {
                    name: 'Scope for fun and profit Advanced',
                    level: 'Introductory',
                    upVoteCount: 0
                },
                {
                    name: 'Well Behaved Controllers',
                    level: 'Intermediate',
                    upVoteCount: 0
                }
            ]

        }

        $scope.upVoteSession = function (session) {
            session.upVoteCount++;
        }
        $scope.downVoteSession = function (session) {
            session.upVoteCount--;
        }
        $scope.handleChange = function (e) {
            console.log(e);
        }
        $scope.btnDisabled = true;
    }
);